# Liskov Substitution

This project demonstrates Liskov substitution.

The "valid" package shows how two classes, Bulldog and Husky, have the same semantic behaviour.  They both output a string to the console.

The "violation" package shows two classes that have methods with similar syntax, but different semantic behaviour.  

Both methods have the same signature but the square sets both sides simultaneously, whereas the rectangle allows you to change them independently.  This means that the classes violate the Liskov substitution principal.  

See also [Liskov substitution principle](https://en.wikipedia.org/wiki/Liskov_substitution_principle) and [Relationship with inheritance](https://en.wikipedia.org/wiki/Subtyping#Relationship_with_inheritance)