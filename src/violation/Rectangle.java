package violation;

public class Rectangle {
	
	private Double height;
	
	private Double width;
	
	public void setHeight(Double newHeight)
	{
		height = newHeight;
	}
	
	public void setWidth(Double newWidth)
	{
		width = newWidth;
	}
	
	public Double area()
	{
		return height * width;
	}
}
