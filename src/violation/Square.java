package violation;

/**
 * A square is a rectangle that has equal width and height  
 */
public class Square extends Rectangle {

	private Double height;
	
	private Double width;
	
	/**
	 *  We try to offer convenience by setting both width and height at the same
	 *  time.  The object is a square so these values will always be equal.
	 */	
	public void setHeight(Double newHeight)
	{
		height = newHeight;
		width = newHeight;
	}
	
	/**
	 *  This breaks the Liskov substitution principal because the classes are
	 *  semantically different.  They behave differently and so are not 
	 *  behavioural subtypes  
	 */
	public void setWidth(Double newWidth)
	{
		height = newWidth;
		width = newWidth;
	}
		
}
