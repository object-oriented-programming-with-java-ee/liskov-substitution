package demo;

import valid.Bulldog;
import valid.Dog;
import valid.Husky;
import violation.Rectangle;
import violation.Square;

public class Main {

	public static void main(String[] args) {
		
		// upcasting, but not necessary
		Dog bulldog = new Bulldog();
		
		// upcasting, but not necessary
		Dog husky = new Husky();
		
		// we can use either object because they are both subtypes of Dog
		useObject(bulldog);		
		useObject(husky);
		
		// Here is the happy path for our violation code
		Rectangle r = new Rectangle();
		r.setHeight(10.0);
		r.setWidth(5.0);
		assert(r.area() == 50.0);
		
		// This demonstrates a violation of the Liskov substitution principal
		Rectangle s = new Square();
		s.setHeight(10.0);
		s.setWidth(5.0);
		// this fails because the setHeight and setWidth functions behave
		// differently, even though they are syntactically the same 
		assert(s.area() == 50.0);

	}
	
	protected static void useObject(Dog dog)
	{
		dog.makeNoise();
	}

}
